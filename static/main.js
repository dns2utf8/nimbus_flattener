window.addEventListener('load', async function() {

const generated = document.querySelector('#generated');
const result = document.querySelector('#result');
const submit = document.querySelector('input[type=submit]');
const generate = document.querySelector('#generate');
const invalid = document.querySelector('#invalid');

const rand = Math.random;

query_server();

submit.addEventListener('click', query_server);

invalid.addEventListener('click', function() {
  let s = '' + rand();
  for (let i = 0; i < 5; ++i) {
    s += ` ${rand()} `;
  }
  generated.value = generated.innerHTML = generated.innerText = s;
  query_server();
});

generate.addEventListener('click', function() {
  const max_depth = 5;
  const max_length = 10;
  
  const a = gen_tree(max_depth);
  
  generated.value = generated.innerHTML = generated.innerText = JSON.stringify(a);
  
  function gen_tree(max_depth) {
    const a = [];
    if (max_depth > 0) {
      for (let i = 0; i < max_length; ++i) {
        if (rand() < 0.2) {
          a.push(gen_tree(max_depth -1));
        } else {
          a.push(Math.round(rand() * 100));
        }
      }
    }
    return a;
  }
});

async function query_server(ev) {
  const contents = generated.value || generated.innerText ||  generated.innerHTML;
  if (ev && ev.preventDefault) {
    ev.preventDefault();
    console.log(['query_server', contents]);
  }
  const response = await fetch('flatten', {
    method: 'POST',
    body: contents,
  });
  const body = await response.text();

  result.innerHTML = result.innerText = body;
}

});
