#![deny(unsafe_code)]

#[macro_use]
extern crate log;
extern crate simple_logger;
#[macro_use]
extern crate prometheus;

use futures::{future, Future, FutureExt, StreamExt};
use prometheus::{register_int_counter, Encoder, TextEncoder};
use std::collections::HashMap;
use std::net::Ipv6Addr;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::{Arc, /*Barrier,*/ Mutex};
use structopt::StructOpt;
use tokio::sync::mpsc;
use warp::ws::{Message, WebSocket};
use warp::{Buf, Filter};

/// Our state of currently connected users.
///
/// - Key is their id
/// - Value is a sender of `warp::ws::Message`
type Users = Arc<Mutex<HashMap<usize, mpsc::UnboundedSender<Result<Message, warp::Error>>>>>;

/// Our global unique user id counter.
static NEXT_USER_ID: AtomicUsize = AtomicUsize::new(1);

#[tokio::main]
async fn main() {
    simple_logger::init_with_level(log::Level::Info).expect("unable to initialize logger");

    let opt = Opt::from_args();
    info!("{:#?}", opt);

    // Keep track of all connected users, key is usize, value
    // is a websocket sender.
    let users = Arc::new(Mutex::new(HashMap::new()));
    // Turn our "state" into a new Filter...
    // TODO WHY NOT COMPILE?
    // let users = warp::any().map(move || users.clone());
    let users = warp::any().map(move || Arc::clone(&users));

    let processed_request = Arc::new(
        register_int_counter!(
            "numbus_flattener_processed_requests_total",
            "processed requests in total"
        )
        .unwrap(),
    );
    let invalid_request = Arc::new(
        register_int_counter!(
            "numbus_flattener_invalid_requests_total",
            "requests that were not parsable"
        )
        .unwrap(),
    );

    let metrics = {
        let metrics = warp::path!("metrics").map(|| {
            let metric_families = prometheus::gather();
            let encoder = TextEncoder::new();

            let mut buffer = Vec::<u8>::new();
            encoder.encode(&metric_families, &mut buffer).unwrap();

            String::from_utf8(buffer.clone()).expect("unable to serialize metrics")
        });
        warp::serve(metrics).run((Ipv6Addr::UNSPECIFIED, opt.metrics_port))
    };

    // GET /ws -> websocket upgrade
    let ws = warp::path("ws")
        // The `ws()` filter will prepare Websocket handshake...
        .and(warp::ws())
        .and(users)
        .map(|ws: warp::ws::Ws, users| {
            // This will call our function if the handshake succeeds.
            ws.on_upgrade(move |socket| user_connected(socket, users).map(|result| result.unwrap()))
        });

    // POST /flatten
    let flatten = {
        let processed_request = processed_request.clone();
        warp::path!("flatten")
            .and(warp::post())
            .and(warp::body::content_length_limit(opt.max_input_len).and(warp::body::aggregate()))
            .map(full_body)
            .map(
                move |body: String| match list_flattener::build_structure(&body) {
                    Ok(parsed) => {
                        processed_request.inc();

                        format!("{:?}\n", parsed.iter().collect::<Vec<_>>())
                    }
                    Err(e) => {
                        invalid_request.inc();
                        format!("no no no: {:?}\n", e)
                    }
                },
            )
    };

    // GET /hello/warp => 200 OK with body "Hello, warp!"
    let hello = {
        let processed_request = processed_request.clone();
        warp::path!("hello" / String).map(move |name| {
            processed_request.inc();
            format!("Hello, {}!", name)
        })
    };

    let routes = ws.or(flatten).or(hello).or(warp::fs::dir("static"));

    let main = warp::serve(routes).run((Ipv6Addr::UNSPECIFIED, opt.port_http));
    futures::join!(main, metrics)
    //.await
;
}

fn full_body(mut body: impl Buf) -> String {
    // It could have several non-contiguous slices of memory...
    let mut buffer = vec![];
    while body.has_remaining() {
        let bytes: &[u8] = body.bytes();
        let len = bytes.len();
        buffer.extend_from_slice(bytes);
        body.advance(len);
    }
    String::from_utf8_lossy(&buffer).to_string()
}

/// A test utility for simulating a database load on disks
#[derive(StructOpt, Debug, Clone)]
struct Opt {
    /// Max input for POST requests
    #[structopt(short="l", long, parse(try_from_str = parse_int::parse), default_value = "16_000_000", env = "NF_MAX_INPUT_LEN")]
    max_input_len: u64,
    ///
    #[structopt(short, long, default_value = "8080", env = "NF_PORT_HTTP")]
    port_http: u16,

    /* TODO
    /// Wether or not to enable the /metrics route on the secondary port
    #[structopt(short, long, env = "NF_COLLECT_METRICS")]
    collect_metrics: bool,
    */
    ///
    #[structopt(short, long, default_value = "9090", env = "NF_METRICS_PORT")]
    metrics_port: u16,
}

fn user_connected(ws: WebSocket, users: Users) -> impl Future<Output = Result<(), ()>> {
    // Use a counter to assign a new unique ID for this user.
    let my_id = NEXT_USER_ID.fetch_add(1, Ordering::Relaxed);

    info!("new chat user: {}", my_id);

    // Split the socket into a sender and receive of messages.
    let (user_ws_tx, user_ws_rx) = ws.split();

    // Use an unbounded channel to handle buffering and flushing of messages
    // to the websocket...
    let (tx, rx) = mpsc::unbounded_channel();
    tokio::task::spawn(rx.forward(user_ws_tx).map(|result| {
        if let Err(e) = result {
            warn!("websocket send error: {}", e);
        }
    }));

    // Save the sender in our list of connected users.
    users.lock().unwrap().insert(my_id, tx);

    // Return a `Future` that is basically a state machine managing
    // this specific user's connection.

    // Make an extra clone to give to our disconnection handler...
    let users2 = users.clone();

    user_ws_rx
        // Every time the user sends a message, broadcast it to
        // all other users...
        .for_each(move |msg| {
            user_message(my_id, msg.unwrap(), &users);
            future::ready(())
        })
        // for_each will keep processing as long as the user stays
        // connected. Once they disconnect, then...
        .then(move |result| {
            user_disconnected(my_id, &users2);
            future::ok(result)
        })
    // If at any time, there was a websocket error, log here...
    // .map_err(move |e| {
    //     warn!("websocket error(uid={}): {}", my_id, e);
    // })
}

fn user_message(my_id: usize, msg: Message, users: &Users) {
    // Skip any non-Text messages...
    let msg = if let Ok(s) = msg.to_str() {
        s
    } else {
        return;
    };

    let new_msg = format!("<User#{}>: {}", my_id, msg);

    // New message from this user, send it to everyone else (except same uid)...
    //
    // We use `retain` instead of a for loop so that we can reap any user that
    // appears to have disconnected.
    for (&uid, tx) in users.lock().unwrap().iter_mut() {
        if my_id != uid {
            match tx.send(Ok(Message::text(new_msg.clone()))) {
                Ok(()) => (),
                Err(_disconnected) => {
                    // The tx is disconnected, our `user_disconnected` code
                    // should be happening in another task, nothing more to
                    // do here.
                }
            }
        }
    }
}

fn user_disconnected(my_id: usize, users: &Users) {
    info!("good bye user: {}", my_id);

    // Stream closed up, so remove from the user list
    users.lock().unwrap().remove(&my_id);
}
