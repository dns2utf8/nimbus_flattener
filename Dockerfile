FROM rust:latest as build

RUN mkdir -p /build-out

COPY ./ ./

RUN cargo build --release

RUN cp target/release/nimbus_flattener /build-out/
RUN cp -r static /build-out/

# Create runtime container
# Note that we need a small init process for PID 1 that forwards signals.
# See https://github.com/Yelp/dumb-init
FROM debian:stable-slim

ENV DEBIAN_FRONTEND=noninteractive
# install watch and top since we are debugging a cluster
RUN apt-get update && apt-get -y install dumb-init procps && rm -rf /var/lib/apt/lists/*

COPY --from=build /build-out/nimbus_flattener /
COPY --from=build /build-out/static /static

#RUN useradd -ms /bin/bash  nimbus_flattener
#USER nimbus_flattener
USER nobody

EXPOSE 8080
EXPOSE 9090

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD /nimbus_flattener
